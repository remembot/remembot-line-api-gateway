# Remembot (Adprog Team 6 Term 2 2018/2019)

[![pipeline status](https://gitlab.com/remembot/remembot-line-api-gateway/badges/master/pipeline.svg)](https://gitlab.com/remembot/remembot-line-api-gateway/commits/master)
[![coverage report](https://gitlab.com/remembot/remembot-line-api-gateway/badges/master/coverage.svg)](https://gitlab.com/remembot/remembot-line-api-gateway//commits/master)

## Team member
- Bimo Iman Smartadi (NPM : 1706039780)  [Active in this Repo]
- Dafa Ramadansyah (NPM : 1706039370)    [Active in this Repo]
- Fadhriz Qadrul Amien (NPM : 1506758065)
- Gema Pratama Aditya (NPM : 1706040031)
- Natasya Meidiana Akhda (NPM :1706979392)

## Description
This Repo is for Line API purposes. All features regarding user interactions to database and push (flex) messages to user are handled here

## Design Pattern Used
	- Builder pattern
    - Decorator pattern
	
	
## References
- http://hibernate.org/orm/
- https://spring.io/blog/2015/07/14/microservices-with-spring
- https://spring.io/guides/gs/consuming-rest/
- https://www.baeldung.com/rest-template
- https://www.baeldung.com/spring-component-repository-service
- https://www.baeldung.com/spring-controllers
- https://www.journaldev.com/7148/java-httpurlconnection-example-java-http-request-get-post
- https://www.mkyong.com/java/how-to-send-http-request-getpost-in-java/
- https://www.baeldung.com/spring-controller-vs-restcontroller
# More Info
This is the API Service, For the other Database Service please refer to the [Database Repo](https://gitlab.com/remembot/remembot-persistence-database)