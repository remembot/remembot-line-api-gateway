package ap6.spring.boot.linegateway.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class ObjectMapperUtility {

	private final ObjectMapper objectMapper = new ObjectMapper();

	public String serialize(Object serializableObject) {
		try {
			return this.objectMapper.writeValueAsString(serializableObject);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e.getCause());
		}
	}

	public Object serializeThenDeserialize(Object serializableObject) {
		try {
			String objectAsJson = this.serialize(serializableObject);
			return this.objectMapper.readValue(objectAsJson, serializableObject.getClass());
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e.getCause());
		}
	}
}
