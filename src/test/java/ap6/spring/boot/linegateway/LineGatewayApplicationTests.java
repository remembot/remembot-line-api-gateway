package ap6.spring.boot.linegateway;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import ap6.spring.boot.linegateway.controller.LineGatewayController;
import ap6.spring.boot.linegateway.controller.ScheduleFlexMessageSupplier;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import ap6.spring.boot.linegateway.utils.EventTestUtility;
import ap6.spring.boot.linegateway.utils.ObjectMapperUtility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(properties = "line.bot.handler.enabled=false", classes = LineGatewayApplication.class)
@ExtendWith(SpringExtension.class)
public class LineGatewayApplicationTests {

    static {
        System.setProperty("line.bot.channelSecret", "DUMMYSECRET");
        System.setProperty("line.bot.channelToken", "DUMMYTOKEN");
    }

    @Autowired
    private LineGatewayController lineGatewayController;

    private final EventTestUtility TEST_EVENT_UTIL = new EventTestUtility();
    private final ObjectMapperUtility MAP_OBJECT_UTIL = new ObjectMapperUtility();

    void testBotModelResponse(Object originalModel) {
        Object reconstructedModel = this.MAP_OBJECT_UTIL.serializeThenDeserialize(originalModel);
        assertThat(reconstructedModel).isEqualTo(originalModel);
    }

    @Test
    void testContextLoads() {
        assertNotNull(lineGatewayController);
    }

    @Test
    void testShowHelpMessage() {
        MessageEvent<TextMessageContent> event = this.TEST_EVENT_UTIL
                .createDummyTextMessage("HELP");
        Message reply = lineGatewayController.handleTextEvent(event);
        this.testBotModelResponse(reply);
    }

    @Test
    void testShowStatusMessage() {
        MessageEvent<TextMessageContent> event = this.TEST_EVENT_UTIL
                .createDummyTextMessage("Status");
        Message reply = lineGatewayController.handleTextEvent(event);
        this.testBotModelResponse(reply);
    }

    @Test
    void testShowTambahWithoutHourResponse() {
        MessageEvent<TextMessageContent> event = this.TEST_EVENT_UTIL
                .createDummyTextMessage("TAMBAH test 20-10-2019");
        Message reply = lineGatewayController.handleTextEvent(event);
        this.testBotModelResponse(reply);
    }

    @Test
    void testShowTambahWithHourResponse() {
        MessageEvent<TextMessageContent> event = this.TEST_EVENT_UTIL
                .createDummyTextMessage("TAMBAH test 20-10-2019 08:00");
        Message reply = lineGatewayController.handleTextEvent(event);

        this.testBotModelResponse(reply);
    }

    @Test
    void testSendPushMessage() {
        Message reply = lineGatewayController.sendPushMessage("tes","name","kuliah");

        this.testBotModelResponse(reply);
    }

    @Test
    void testShowHapusResponse() {
        MessageEvent<TextMessageContent> event = this.TEST_EVENT_UTIL
                .createDummyTextMessage("HAPUS test");
        Message reply = lineGatewayController.handleTextEvent(event);
        this.testBotModelResponse(reply);
    }

    @Test
    void testShowJadwalResponse() {
        Message reply = new ScheduleFlexMessageSupplier().get("name","1999-5-15T20:30:");
        MessageEvent<TextMessageContent> event = this.TEST_EVENT_UTIL
                .createDummyTextMessage("JADWAL");

        this.testBotModelResponse(reply);
    }

    @Test
    void testHandleInvalidTextMessageEvent() {
        MessageEvent<TextMessageContent> event = this.TEST_EVENT_UTIL
                .createDummyTextMessage("random");
        Message reply = lineGatewayController.handleTextEvent(event);
        assertEquals("Terimakasih sudah meng-Add Remembot!, ketik HELP untuk info keyword yang ada",
                ((TextMessage) reply).getText());
    }
}
