package ap6.spring.boot.linegateway.controller;

import static java.util.Arrays.asList;


import java.util.Collections;
import java.util.function.Supplier;

import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Button.ButtonHeight;
import com.linecorp.bot.model.message.flex.component.Button.ButtonStyle;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Image.ImageAspectMode;
import com.linecorp.bot.model.message.flex.component.Image.ImageAspectRatio;
import com.linecorp.bot.model.message.flex.component.Image.ImageSize;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.unit.*;

public class ScheduleFlexMessageSupplier implements Supplier<FlexMessage> {
    @Override
    public FlexMessage get() {
        final Separator separator = Separator.builder().margin(FlexMarginSize.XXL).build();
        final Box headerBlock = createBodyHeaderBox();
        final Box itemBlock = createBodyItemBlock();
        final Box summaryBlock = createBodySummaryBlock();
        final Box footerBlock = createBodyFooterBlock();

        final Box bodyBlock = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(
                        headerBlock,
                        separator,
                        itemBlock,
                        separator,
                        summaryBlock,
                        separator,
                        footerBlock))
                .build();

        final Bubble bubble = Bubble.builder()
                .body(bodyBlock)
                .build();
        return new FlexMessage("Your Schedule", bubble);
    }

    public FlexMessage get(String eventName, String eventDate) {
        final Separator separator = Separator.builder().margin(FlexMarginSize.XXL).build();
        final Box headerBlock = createBodyHeaderBox();
        final Box itemBlock = createBodyItemBlock(eventName, eventDate);
        final Box summaryBlock = createBodySummaryBlock();
        final Box footerBlock = createBodyFooterBlock();

        final Box bodyBlock = Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(
                        headerBlock,
                        separator,
                        itemBlock,
                        separator,
                        summaryBlock,
                        separator,
                        footerBlock))
                .build();

        final Bubble bubble = Bubble.builder()
                .body(bodyBlock)
                .build();
        return new FlexMessage("Your Schedule", bubble);
    }

    private Box createBodyHeaderBox() {
        final Text bodyHeaderText = Text.builder()
                .text("Your Schedule")
                .weight(Text.TextWeight.BOLD)
                .color("#1db446")
                .size(FlexFontSize.SM)
                .build();
        final Text bodyTitleHeaderText = Text.builder()
                .text("TODAY")
                .weight(Text.TextWeight.BOLD)
                .size(FlexFontSize.XXL)
                .margin(FlexMarginSize.MD)
                .build();
        final Text bodyTitleHeaderDetail = Text.builder()
                .text("Tuesday, 14 May 2019")
                .size(FlexFontSize.XS)
                .color("#aaaaaa")
                .wrap(true)
                .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(
                        bodyHeaderText,
                        bodyTitleHeaderText,
                        bodyTitleHeaderDetail))
                .build();
    }

    private Box createBodyItemBlock() {
        final Separator separator = Separator.builder().margin(FlexMarginSize.XXL).build();
        final Box item1 = createItem("Kuliah" , "8:00 A.M");
//        final Box item2 = createItem("Daily Kuliah" , "8:00 A.M - 2:15 P.M");
//        final Box item3 = createItem("Meeting with Client" , "3:00 P.M");
        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .margin(FlexMarginSize.XXL)
                .spacing(FlexMarginSize.SM)
                .contents(asList(
                        item1))
                .build();
    }

    private Box createBodyItemBlock(String eventName, String eventDate) {
        final Separator separator = Separator.builder().margin(FlexMarginSize.XXL).build();
        String date = eventDate.substring(11,15);
        final Box item1 = createItem(eventName , date);
        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .margin(FlexMarginSize.XXL)
                .spacing(FlexMarginSize.SM)
                .contents(asList(
                        item1))
                .build();
    }


    private Box createBodySummaryBlock() {
        final Separator separator = Separator.builder().margin(FlexMarginSize.XXL).build();
        final  Box items = createItemBold( "Tomorrow ", "   " );
        final  Box total = createItem( "Deadline Basdut " , " 10.59 P.M " );
        final  Box cash = createItem( "Deadline Adprog" , "10.50 P.M" );
        final  Box change = createItem( "DEAD" , " All Day " );

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .margin(FlexMarginSize.XXL)
                .spacing(FlexMarginSize.SM)
                .contents(asList(
                        items,
                        total,
                        cash,
                        change))
                .build();
    }

    private Box createBodyFooterBlock() {
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .margin(FlexMarginSize.MD)
                .contents(asList(
                        Text.builder()
                                .text("Click here for more")
                                .size(FlexFontSize.XS)
                                .color("#aaaaaa")
                                .flex(0)
                                .build(),
                        Text.builder()
                                .text("    ")
                                .size(FlexFontSize.XS)
                                .color("#aaaaaa")
                                .align(FlexAlign.END)
                                .build()
                ))
                .build();
    }

    private Box createItem(String name, String price) {
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(asList(
                        Text.builder()
                                .text(name)
                                .size(FlexFontSize.SM)
                                .color("#555555")
                                .flex(0)
                                .build(),
                        Text.builder()
                                .text(price)
                                .size(FlexFontSize.SM)
                                .color("#111111")
                                .align(FlexAlign.END)
                                .build()
                )).build();
    }

    private Box createItemBold(String name, String price) {
        return Box.builder()
                .layout(FlexLayout.HORIZONTAL)
                .contents(asList(
                        Text.builder()
                                .text(name)
                                .weight(Text.TextWeight.BOLD)
                                .size(FlexFontSize.SM)
                                .color("#555555")
                                .flex(0)
                                .build(),
                        Text.builder()
                                .text(price)
                                .size(FlexFontSize.SM)
                                .color("#111111")
                                .align(FlexAlign.END)
                                .build()
                )).build();
    }
}