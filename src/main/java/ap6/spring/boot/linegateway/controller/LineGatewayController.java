package ap6.spring.boot.linegateway.controller;

import ap6.spring.boot.linegateway.url.DeleteCreatorURL;
import ap6.spring.boot.linegateway.url.EventCreatorUrl;
import ap6.spring.boot.linegateway.service.DatabaseService;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
//import org.apache.commons.io.IOUtils;
//import org.codehaus.jackson.map.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.web.bind.annotation.*;

@Controller
@LineMessageHandler
public class LineGatewayController {
    @Autowired
    private LineMessagingClient lineMessagingClient;
    private String eventName;
    private String eventDate;

    @EventMapping
    public Message handleTextEvent(MessageEvent<TextMessageContent> messageEvent){
        String message = messageEvent.getMessage().getText().toLowerCase();
        String[] messageSplit = message.split(" |\n");
        String command = messageSplit[0];
        String answer = "";

        String username = "";
        String id = "";
        try {
            username = lineMessagingClient.getProfile(messageEvent.getSource().getUserId()).get().getDisplayName();
            id = messageEvent.getSource().getUserId();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Something went wrong");
        }
        if(command.equalsIgnoreCase("tambah")){
            try {
                //Todo simpan database
                EventCreatorUrl event = null;
                if (messageSplit.length > 3) {
                    event = new EventCreatorUrl(username, messageSplit[1], messageSplit[2] + messageSplit[3],id);
                } else {
                    event = new EventCreatorUrl(username, messageSplit[1], messageSplit[2] + "00:00",id);
                }
                DatabaseService.sendPost(event.getURL(), "create");

                answer = "Tersimpan!\nJadwal : " + messageSplit[1] + "\nTanggal : " + messageSplit[2]
                        + ((messageSplit.length > 3) ? "\nJam : " + messageSplit[3] : "");
            } catch (Exception e) {
                answer = "Gagal menambahkan jadwal";
            }
        } else if (command.equalsIgnoreCase("hapus")) {
            try {
                //Todo hapus database
                DeleteCreatorURL deletor = new DeleteCreatorURL(messageSplit[0], id);
                DatabaseService.sendPost(deletor.getURL(), "delete");
                //
                answer = "Jadwal " + messageSplit[1] + " berhasil terhapus!";
            } catch (Exception e) {
                answer = "Gagal menghapus jadwal";
            }
        } else if (command.equalsIgnoreCase("status")) {
            try {
                //Todo cek jadwal
                List<String[]> response = DatabaseService.sendGet(id);

                eventName = response.get(0)[0];
                eventDate = response.get(0)[1];
                answer = "Jadwal ada! Ketik JADWAL untuk melihatnya";
            } catch (Exception e) {
                answer = "Tidak ada jadwal!";
            }
        } else if (command.equalsIgnoreCase("help")) {
            answer = "- TAMBAH untuk menambah jadwal, " +
                    "\nformat\nTAMBAH\n<nama jadwal>\n<tanggal jadwal format YYYY-MM-DD>" +
                    "\n<jam jadwal format HH:mm> (Opsional)\n"
                    + "- HAPUS untuk menghapus jadwal yang ada"
                    + "\nformat\nHAPUS\n<nama jadwal>\n"
                    + "- STATUS untuk mengecek jadwal yang ada"
                    + "\nformat\nSTATUS\n";
        } else if (command.equalsIgnoreCase("jadwal")){
            return new ScheduleFlexMessageSupplier().get(eventName, eventDate);
        } else {
            answer = "Terimakasih sudah meng-Add Remembot!, ketik HELP untuk info keyword yang ada";
        }

        String replyToken = messageEvent.getReplyToken();
        replyMessage(replyToken, answer);
        return new TextMessage(answer);
    }

    @RequestMapping(value = "/push", method = RequestMethod.POST)
    @ResponseBody
    public Message sendPushMessage(@RequestParam(required = false) String userId, String username, String eventName) {
        String text = "";
        if (userId != null) {
            text = "Hey "+ username +"!\nJangan lupa ada " + eventName + " Hari ini!";
            Message message = new TextMessage(text);
            pushMessage(userId, message);
            return message;
        } else {
            System.out.println("Something went wrong");
        }
        return new TextMessage("failed");
    }

    private Message replyMessage(String replyToken, String answer) {
        TextMessage textMessageAnswer = new TextMessage(answer);
        try {
            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, textMessageAnswer))
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Something went wrong");
        }
        return new TextMessage(answer);
    }
    private Message pushMessage(String userId, Message message) {
        // how to get userId messageEvent.getSource().getUserId();
        try {
            lineMessagingClient
                    .pushMessage(new PushMessage(userId, message)).get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Something went wrong");
        }
        return message;
    }

}
