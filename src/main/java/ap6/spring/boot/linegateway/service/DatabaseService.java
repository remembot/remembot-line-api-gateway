package ap6.spring.boot.linegateway.service;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.tomcat.util.json.JSONParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


public class DatabaseService {
    private static final String USER_AGENT = "Mozilla/5.0";

    public static void sendPost(String urlParameters, String command) throws Exception {
        String url = "https://dbkap6.herokuapp.com/event/" + command;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Content-Type", "application/json");
        System.out.println("url param : " + urlParameters);
        // For POST only - START
        con.setDoOutput(true);
        DataOutputStream os = new DataOutputStream(con.getOutputStream());
        os.writeBytes(urlParameters);
        os.flush();
        os.close();
        // For POST only - END

        int responseCode = con.getResponseCode();
        System.out.println("POST Response Code :: " + responseCode);

        if (responseCode != HttpURLConnection.HTTP_OK) { //success
            throw new Exception("POST Request Failed");
        }
    }

    public static List<String[]> sendGet(String id) throws Exception {

        String url = "http://dbkap6.herokuapp.com/event/events";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("Content-Type", "application/json");

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);
        if (responseCode != HttpURLConnection.HTTP_OK) {
            throw new Exception("Cannot Send get");
        }

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        String json = response.substring(1, response.length()-1);
        System.out.println(json);
        JsonParser parser = new JsonParser();
        JsonArray jsonArr = parser.parse(json).getAsJsonArray();
        System.out.println("this is arr : " + jsonArr);
        List<String[]> listData = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            String[] parsedData = new String[2];
            JsonObject j = jsonArr.get(i).getAsJsonObject();
            if (j.get("id").getAsString().equals(id)) {
                parsedData[0] = j.get("name").getAsString();
                parsedData[1] = j.get("startDate").getAsString();
            }
            System.out.println("name " + parsedData[0]);
            System.out.println("date " + parsedData[1]);
        }

        return listData;
    }

}
