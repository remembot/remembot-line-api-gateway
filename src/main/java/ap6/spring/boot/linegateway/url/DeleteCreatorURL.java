package ap6.spring.boot.linegateway.url;

public class DeleteCreatorURL implements URLCreatable{
    private String eventName;
    private String requestCode;
    private String userId;

    public DeleteCreatorURL(String eventName, String id) {
        this.eventName = eventName;
        this.userId = id;
        setRequestCode();
    }

    @Override
    public void setRequestCode() {
        this.requestCode = "Delete";
    }

    public String getURL() {
        return "senderDisplayName= "+ userId + "&name=" + eventName;
    }
}
