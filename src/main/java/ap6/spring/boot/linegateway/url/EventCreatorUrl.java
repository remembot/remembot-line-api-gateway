package ap6.spring.boot.linegateway.url;

public class EventCreatorUrl implements URLCreatable{
    private String eventName;
    private String eventDate;
    private String requestCode;
    private String userId;
    private String username;

    public EventCreatorUrl(String username, String eventName, String date, String id) {
        this.eventName = eventName;
        this.username = username;
        this.eventDate = date;
        this.userId = id;
        setRequestCode();
    }

    @Override
    public void setRequestCode() {
        this.requestCode = "Event";
    }

    public String getURL() {
        return "senderDisplayName=" + userId +"&name=" + eventName + "&startDate="
                + eventDate + ":00.000+0000";
    }
}
